package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"
)

const (
	PicDir         = "/Users/shortlog/Pictures/WallPapers/"
	BaseURL        = "https://cn.bing.com"
	HPImageArchive = "/HPImageArchive.aspx?format=js&idx=%d&n=1&nc=%d&pid=hp"
	GlobalArchive  = "/HPImageArchive.aspx?format=js&idx=%d&n=1&nc=%d&pid=hp&ensearch=1"
	WallpaperCmd   = `tell application "Finder" to set desktop picture to POSIX file "%s"`
)

type ImgResp struct {
	Images []*Image `json:"images,omitempty"`
}

type Image struct {
	URL       string `json:"url,omitempty"`
	CopyRight string `json:"copyright,omitempty"`
}

func main() {
	list := flag.Bool("l", false, "list pictures")
	index := flag.Int("i", -1, "pic index")
	bing := flag.Bool("b", false, "download wallpaper from bing")
	global := flag.Bool("g", false, "query global pic")
	flag.Parse()
	var (
		name string
		err  error
	)
	switch {
	case *list:
		listPicDir()
		return
	case *bing:
		if name, err = getBingWallpaper(*index, *global); err != nil {
			return
		}
	case *index > 0:
		if name, err = selectPic(*index); err != nil {
			return
		}
	default:
		if name, err = randomPic(); err != nil {
			return
		}
	}
	log.Println("name:", name)
	if err = setOSXWallpaper(name); err != nil {
		logErr(err, "setOSXWallpapers")
	}
}

func listPicDir() {
	files, err := os.ReadDir(PicDir)
	if err != nil {
		logErr(err, "os.ReadDir")
		return
	}
	for i, file := range files {
		log.Println(i, file.Name())
	}
}

func getBingWallpaper(index int, global bool) (name string, err error) {
	if index < 0 {
		index = 0
	}
	var url string
	if global {
		url = BaseURL + fmt.Sprintf(GlobalArchive, index, time.Now().UnixNano()/1e6)
	} else {
		url = BaseURL + fmt.Sprintf(HPImageArchive, index, time.Now().UnixNano()/1e6)
	}
	ir := &ImgResp{}
	err = getJSON(url, &ir)
	if err != nil {
		logErr(err, "getJSON")
		return
	}
	if len(ir.Images) == 0 {
		log.Println("no images")
		return
	}
	l := strings.IndexByte(ir.Images[0].CopyRight, '(')
	if l == -1 {
		l = strings.IndexByte(ir.Images[0].CopyRight, '©')
	}
	name = PicDir + strings.ReplaceAll(ir.Images[0].CopyRight[:l-1], "/", "_") + ".jpg"
	if exists(name) {
		log.Println("pic exists")
		return
	}
	url = BaseURL + ir.Images[0].URL
	resp, err := http.DefaultClient.Get(url)
	if err != nil {
		logErr(err, "http.DefaultClient.Get")
		return
	}
	defer resp.Body.Close()
	data, err := io.ReadAll(resp.Body)
	if err != nil {
		logErr(err, "io.ReadAll")
		return
	}
	if err = os.WriteFile(name, data, os.ModePerm); err != nil {
		logErr(err, "os.WriteFile")
	}
	return
}

func exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func setOSXWallpaper(name string) error {
	return exec.Command("/usr/bin/osascript", "-e", fmt.Sprintf(WallpaperCmd, name)).Run()
}

func selectPic(index int) (name string, err error) {
	files, err := os.ReadDir(PicDir)
	if err != nil {
		logErr(err, "os.ReadDir")
		return
	}
	switch strings.ToLower(path.Ext(files[index].Name())) {
	case ".jpg", ".jpeg", ".png":
		name = PicDir + files[index].Name()
	default:
		err = fmt.Errorf("index %d is not a picture, use -list", index)
		logErr(err, "selectPic")
	}
	return
}

func randomPic() (name string, err error) {
	rand.Seed(time.Now().UnixNano())
	files, err := os.ReadDir(PicDir)
	if err != nil {
		logErr(err, "os.ReadDir")
		return
	}
	list := make([]string, 0)
	for _, file := range files {
		name := file.Name()
		switch strings.ToLower(path.Ext(name)) {
		case ".jpg", ".jpeg", ".png":
			list = append(list, name)
		}
	}
	name = PicDir + list[rand.Intn(len(list))]
	return
}

func logErr(err error, msg string) {
	if err == nil {
		return
	}
	log.Printf("[ERR] %s err: %v", msg, err)
}

func getJSON(url string, data interface{}) (err error) {
	resp, err := http.DefaultClient.Get(url)
	if err != nil {
		return
	}
	if resp.Body != nil {
		defer resp.Body.Close()
	}
	err = json.NewDecoder(resp.Body).Decode(&data)
	return
}
